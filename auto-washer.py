#!/usr/bin/env python

#Insperation for the script was found on https://askubuntu.com/questions/518457/autostart-program-whenever-a-file-is-added-to-a-folder 
# 
#This script looks at the downloads folder in the home directory and secBrowser directory and automatically cleans meta data from the files. 
#This script only works for the files supported by mat2. For more information read the mat2 manual. 

import subprocess
import time
import os

username = os.getlogin() #get the username of the user logged in. 

#the below commands set the directories to search through and also sets the command and flag
home_downloads = "/home/"+username+"/Downloads" 
secBrowser_downloads = "/home/"+username+"/.secbrowser/secbrowser/Browser/Downloads"
washer = "mat2"
flag = "--inplace" #this flag sets mat2 to clean a file and not create a copy. 

#the 2 get functions get the files in the given directories
def get_drlist_home(): 
	return subprocess.check_output(["ls", home_downloads]).decode('utf-8').strip().split("\n")
def get_drlist_secBrowser():
	return subprocess.check_output(["ls", secBrowser_downloads]).decode('utf-8').strip().split("\n")


#Gets a list from the directory, then waits, then gets another list to compare. 
while True:
	drlist1_home = get_drlist_home()
	drlist1_secBrowser = get_drlist_secBrowser()
	fileOpen.close()
	time.sleep(2)
	drlist2_home = get_drlist_home()
	drlist2_secBrowser = get_drlist_secBrowser()
#if there is a extra file in drlist2 then the command is called to clean the meta data of said file. `
	for home_file in [f for f in drlist2_home if not f in drlist1_home]:
		if home_file != "":
			command = washer+" "+home_downloads+"/'"+home_file+"' "+flag #quotes aroun home_file allows for documents with spaces to be processed
			command2 = "clamscan -i "+home_downloads+"/'"+home_file+"' >> /home/"+username+"/scanReports/'"+home_file+"'.txt"
			subprocess.Popen(["/bin/bash", "-c", command])
			subprocess.Popen(["/bin/bash", "-c", command2])
		else: 
			pass
	for secBrowser_file in [f for f in drlist2_secBrowser if not f in drlist1_secBrowser]:
		if secBrowser_file != "":
			command = washer+" "+secBrowser_downloads+"/'"+secBrowser_file+"' "+flag
			command2 = "clamscan -i "+secBrowser_downloads+"/'"+secBrowser_file+"' >> /home/"+username+"/scanReports/'"+secBrowser_file+"'.txt "
			subprocess.Popen(["/bin/bash", "-c", command])
			subprocess.Popen(["/bin/bash", "-c", command2])
		else:
			pass
