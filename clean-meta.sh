#!/bin/bash

#This script cleans meta data from files in the directory given.   

dirToClean=$1 				#Takes in the argument of the directory you want to clean
cd $dirToClean
fileList=($(find . -maxdepth 1 -type f)) 
echo "Files in the Directory"
echo
echo ${fileList[@]}
echo 
read -p "Do you want to clean a single file (S) or all files (A) in this directory? (S|A): " fileClean
if [ $fileClean = "S" ]   		#If you want to clean individual files goes here
	then
	anotherFile="Y"
	while [ $anotherFile = "Y" ]
	do
		read -p "Enter the full file name you want to clean: " selectedFile
		mat2 $selectedFile --inplace
		read -p "Your file has been cleaned. Do you want to clean another? (Y|N)" anotherFile 
	done
elif [ $fileClean = "A" ]   #cleans all files in the directory
	then
	mat2 ${fileList[@]} --inplace

else
	echo "That is not a valid option"
fi	
