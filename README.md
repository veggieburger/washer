# washer

Information on clean-meta.sh
-----------------------------------------------------------------------------
This script cleans all meta data from files using the tool mat2. 
Before running the script make sure mat2 is downloaded. Information can be found at https://0xacab.org/jvoisin/mat2 

This scripts works by taking in the directory you want to clean, then selecting either individual files to clean or the entire direcotry 

Example command:
	clean-meta.sh /path/to/directory

Information on auto-washer.py
-----------------------------------------------------------------------------
This python script looks at the Downloads folder in the users home directory and secBrowsers home directory and scans newly downloaded files for viruses as well as cleans metadata using mat2. 
 
**If you do not use secBrowser there is no need for the secBrowser portions of the script and can be commented out.**  
To start the script as an automatic process on login. Do the following:  
	1. Give the file executable permissions. I prefer the chmod 711  
	2. Add the script to start up automatically on login.   
	    	I use Parrot OS, and searched for Autostart. Under Script File I added the path to auto-washer.py and set it to run on Startup.   
	3. Your done. Restart your computer and test it out by adding files to the respective downloads folder   

Issues:   
	Currently the script only works for one file being downloaded at a time. If multiple files are downloaded at the same time, the script will only clean the meta data on one file
